<?php

declare(strict_types=1);

namespace Drupal\popup_message\Enum;

/**
 * Contains default values for popup message.
 */
enum PopupMessageDefaultValues: int {
  case POPUP_MESSAGE_DEFAULT_SIZE = 300;
}
